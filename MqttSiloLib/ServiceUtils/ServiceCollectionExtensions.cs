﻿namespace MqttSiloLib.ServiceUtils;

using Microsoft.Extensions.DependencyInjection;
using System;

/// <summary>
/// Contains extension methods for <see cref="IServiceCollection"/>.
/// </summary>
public static class ServiceCollectionExtensions
{
    /// <summary>
    /// Adds a singleton service of type <see cref="IServiceFactory{TService}"/> to the given service collection and
    /// adds a transient service of type <typeparamref name="TService"/> to the service collection.
    /// </summary>
    /// <typeparam name="TService">The service type.</typeparam>
    /// <param name="services">The service collection.</param>
    /// <returns>The service collection.</returns>
    public static IServiceCollection AddFactory<TService>(this IServiceCollection services) where TService : class
    {
        ArgumentNullException.ThrowIfNull(services);
        services.AddTransient<TService>();
        services.AddSingleton<IServiceFactory<TService>, ServiceProviderServiceFactory<TService>>();
        return services;
    }
}
