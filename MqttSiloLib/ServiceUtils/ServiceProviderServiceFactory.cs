﻿namespace MqttSiloLib.ServiceUtils;

using Microsoft.Extensions.DependencyInjection;
using System;

/// <summary>
/// Uses a <see cref="IServiceProvider"/> to create objects of the given service type.
/// </summary>
/// <typeparam name="TService">The service type.</typeparam>
public class ServiceProviderServiceFactory<TService> : IServiceFactory<TService>
{
    private readonly IServiceProvider _serviceProvider;

    /// <summary>
    /// Constructs the factory with the given service provider.
    /// </summary>
    /// <param name="serviceProvider">The service provider.</param>
    /// <exception cref="ArgumentNullException"><paramref name="serviceProvider"/> is null.</exception>
    public ServiceProviderServiceFactory(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider ?? throw new ArgumentNullException(nameof(serviceProvider));
    }

    /// <inheritdoc/>
    public (TService Service, IDisposable Lifetime) Create(params object[] parameters)
    {
        var scope = _serviceProvider.CreateScope();
        try
        {
            var service = ActivatorUtilities.CreateInstance<TService>(scope.ServiceProvider, parameters);
            return (service, scope);
        }
        catch
        {
            scope.Dispose();
            throw;
        }
    }
}
