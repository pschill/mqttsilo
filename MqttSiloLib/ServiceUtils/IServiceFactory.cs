﻿namespace MqttSiloLib.ServiceUtils;

using System;

/// <summary>
/// Factory that creates objects of the given service type.
/// </summary>
/// <typeparam name="TService">The service type.</typeparam>
public interface IServiceFactory<TService>
{
    /// <summary>
    /// Creates the service with constructor arguments provided directly and / or from within the factory.
    /// </summary>
    /// <param name="parameters">Constructor arguments not provided by the factory.</param>
    /// <returns>The service and a disposable object that, when disposed, disposes the service and its dependencies. The disposable object may be null if neither the service nor its dependencies must be disposed.</returns>
    (TService Service, IDisposable? Lifetime) Create(params object[] parameters);
}

/// <summary>
/// Contains extension methods for the <see cref="IServiceFactory{TService}"/>.
/// </summary>
public static class ServiceFactoryExtensions
{
    /// <summary>
    /// Creates the service with constructor arguments provided directly and / or from within the factory.
    /// </summary>
    /// <typeparam name="TService">The service type.</typeparam>
    /// <param name="serviceFactory">The service factory.</param>
    /// <param name="service">When this method returns, contains the created service.</param>
    /// <param name="parameters">Constructor arguments not provided by the factory.</param>
    /// <returns>A disposable object that, when disposed, disposes the service and its dependencies. May be null if neither the service nor its dependencies must be disposed.</returns>
    public static IDisposable? Create<TService>(this IServiceFactory<TService> serviceFactory, out TService service, params object[] parameters)
    {
        ArgumentNullException.ThrowIfNull(serviceFactory);
        (service, var lifetime) = serviceFactory.Create(parameters);
        return lifetime;
    }
}
