namespace MqttSiloApp;

using System;
using Avalonia.Controls;
using Avalonia.Controls.Templates;
using MqttSiloApp.ViewModels;

public class ViewLocator : IDataTemplate
{
    public IControl Build(object data)
    {
        ArgumentNullException.ThrowIfNull(data);
        var name = data.GetType().FullName!.Replace("ViewModel", "View");
        var type = Type.GetType(name);
        if (type is null)
        {
            return new TextBlock { Text = "Not Found: " + name };
        }
        return (Control)Activator.CreateInstance(type)!;
    }

    public bool Match(object data)
    {
        return data is ViewModelBase;
    }
}
